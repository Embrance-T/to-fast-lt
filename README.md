# to-fast-lt

前端业务代码工具库 （持续更新）

> 提高前端业务代码工作效率

## 安装使用

```bash
$ npm install --save-dev to-fast-lt
```

```javascript
// 完整引入
import toFast from 'to-fast-lt'
console.log('是否为空对象：', toFast.isEmptyObject({ a: 123 })) // false
```

## :package: API 文档

### Array

#### &emsp;&emsp;[arrayEqual](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/array/arrayEqual.js)&emsp;&emsp;判断两个数组是否相等

### Object

#### &emsp;&emsp;[deepClone](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/deepClone.js)&emsp;&emsp;深拷贝，支持常见类型

#### &emsp;&emsp;[isEmptyObject](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isEmptyObject.js)&emsp;&emsp;判断 Object 是否为空

#### &emsp;&emsp;[isPlainObject](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isPlainObject.js)&emsp;&emsp;判断 Object 是否纯粹对象{ }

#### &emsp;&emsp;[isObject](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isObject.js)&emsp;判断是否为对象类型

#### &emsp;&emsp;[isArray](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isArray.js)&emsp;判断是否为数组类型

 #### &emsp;&emsp;[isBoolean](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isBoolean.js)&emsp;判断是否为布尔类型

 #### &emsp;&emsp;[isString](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isString.js)&emsp;判断是否为 String类型

#### &emsp;&emsp;[isNumber](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isNumber.js)&emsp;判断是否为 Number类型

#### &emsp;&emsp;[isUndefined](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isUndefined.js)&emsp;判断是否为 Undefined类型

#### &emsp;&emsp;[isNull](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isNull.js)&emsp;判断是否为 Null类型

#### &emsp;&emsp;[isFunction](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isFunction.js)&emsp;判断是否为函数类型

#### &emsp;&emsp;[isSymbol](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isSymbol.js)&emsp;判断是否为Symbol类型

#### &emsp;&emsp;[isRegExp](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isRegExp.js)&emsp;判断是否为正则类型

#### &emsp;&emsp;[isDate](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isDate.js)&emsp;判断是否为 Date类型

#### &emsp;&emsp;[isMath](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isMath.js)&emsp;判断是否为 Math类型

#### &emsp;&emsp;[isArrayBuffer](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isArrayBuffer.js)&emsp;断是否为 buffer类型

#### &emsp;&emsp;[isBlob](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isBlob.js)&emsp;断是否为 Blob类型

#### &emsp;&emsp;[isFinite](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isFinite.js)&emsp;断是否为 数字类型

#### &emsp;&emsp;[isWeixn](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isWeixn.js)&emsp;&emsp;判断是否为微信浏览器

#### &emsp;&emsp;[isMobile](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/isMobile.js)&emsp;&emsp;判断浏览器环境是pc还是移动端

#### &emsp;&emsp;[getBrowser](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/getBrowser.js)&emsp;&emsp;浏览器判断

#### &emsp;&emsp;[getOS](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/getOS.js)&emsp;&emsp;获取当前操作系统

#### &emsp;&emsp;[copyValue](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/copyValue.js)&emsp;&emsp;复制内容

#### &emsp;&emsp;[arrRemoval](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/arrRemoval.js)&emsp;&emsp;针对数组中某个对象中的字段去重

#### &emsp;&emsp;[getRandomID](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/getRandomID.js)&emsp;&emsp;生成随机ID

#### &emsp;&emsp;[getRandomStr](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/getRandomStr.js)&emsp;&emsp;生成随机字符

#### &emsp;&emsp;[injectScript](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/injectScript.js)&emsp;&emsp;动态引入js

#### &emsp;&emsp;[downloadUrl](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/object/downloadUrl.js)&emsp;&emsp;下载文件

### String

#### &emsp;&emsp;[isJSON](https://gitee.com/Embrance-T/to-fast-lt/blob/master/src/string/isJSON.js)&emsp;&emsp;判断 str 是否为 json格式