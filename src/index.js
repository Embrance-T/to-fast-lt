/**
 * @desc webpack打包入口文件  
 * @example 自动引入子目录下所有js文件
 */
let moduleExports = {};

// https://www.cnblogs.com/chuanmin/p/13530596.html
// require.context()返回的是一个webpack上下文环境、本质是一个函数、有id、keys、resolve属性
const r = require.context('./', true, /^\.\/.+\/.+\.js$/);
r.keys().forEach(key => {
  let attr = key.substring(key.lastIndexOf('/') + 1, key.lastIndexOf('.'));
  moduleExports[attr] = r(key).default; // .default是因为每个封装的js是default方式导出
});
console.log('封装的所有方法:', moduleExports)

export default moduleExports