/**
 *
 *
 * @desc 判断是否为函数类型
 * @param {*} a
 * @return {*} 
 */
export default function isFunction(a) {
  let type = Object.prototype.toString.call(a)
  return type == '[object Function]'
}