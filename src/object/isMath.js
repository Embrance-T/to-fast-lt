/**
 *
 *
 * @desc 判断是否为 Math类型
 * @param {*} a
 * @return {*} 
 */
export default function isMath(a) {
  let type = Object.prototype.toString.call(a);
  return type == '[object Math]';
}