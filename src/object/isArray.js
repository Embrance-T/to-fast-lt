/**
 *
 *
 * @desc 判断是否为数组类型
 * @param {*} arr
 * @return {*} 
 */
export default function isArray(arr) {
  let type = Object.prototype.toString.call(arr);
  return type == '[object Array]';
}