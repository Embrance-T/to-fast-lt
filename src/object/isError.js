/**
 *
 *
 * @desc 判断是否为正则类型
 * @param {*} a
 * @return {*} 
 */
 export default function isRegExp(a) {
  let type = Object.prototype.toString.call(a)
  return type == '[object RegExp]';
}