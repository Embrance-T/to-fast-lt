/**
 *
 *
 * @desc 判断是否为 Number类型
 * @param {*} num
 * @return {*} 
 */
 export default function isNumber(num) {
  let type = Object.prototype.toString.call(num);
  return type == '[object Number]';
}