/**
 *
 *
 * @desc 判断是否为 数字类型
 * @param {*} a
 * @return {*} 
 */
export default function isFinite(a) {
  return Number.isFinite(a)
}