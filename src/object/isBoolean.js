/**
 *
 *
 * @desc 判断是否为布尔类型
 * @param {*} boolean
 * @return {*} 
 */
 export default function isBoolean(boolean) {
  let type = Object.prototype.toString.call(boolean);
  return type == '[object Boolean]';
}