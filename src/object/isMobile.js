/**
 * @description 判断浏览器环境是pc还是移动端
 * @return Boolean true:移动端 false pc
 */
export default function isMobile() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    return true;
  } else {
    return false;
  }
}