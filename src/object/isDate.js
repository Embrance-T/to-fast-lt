/**
 *
 *
 * @desc 判断是否为日期类型
 * @param {*} a
 * @return {*} 
 */
export default function isDate(a) {
  let type = Object.prototype.toString.call(a);
  return type == '[object Date]';
}