/**
 *
 *
 * @desc 判断是否为 Null类型
 * @param {*} a
 * @return {*} 
 */
export default function isNull(a) {
  let type = Object.prototype.toString.call(a);
  return type == '[object Null]';
}