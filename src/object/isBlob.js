/**
 *
 *
 * @desc 判断是否为 Blob类型
 * @param {*} a
 * @return {*} 
 */
export default function isBlob(a) {
  return a instanceof Blob
}