/**
 *
 *
 * @desc 生成随机ID
 * @return {*} 
 */
export default function getRandomID() {
  return Number(Math.random().toString().substr(2, 3) + Date.now()).toString(10)
}