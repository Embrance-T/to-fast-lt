/**
 *
 *
 * @desc 判断是否为对象类型
 * @param {*} obj
 * @return {*} 
 */
export default function isObject(obj) {
  let type = Object.prototype.toString.call(obj);
  return type == '[object Object]';
}