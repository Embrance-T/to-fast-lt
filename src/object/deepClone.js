/**
 *
 * @desc 深拷贝（常见类型）
 * @param {Any} obj
 * @return {Any} 
 */
function deepClone(obj) {
  var newObj;
  // 拿到对象以外的类型
  if (null == obj || 'object' != typeof obj) {
    return obj
  }

  // Date类型
  if (obj instanceof Date) {
    newObj = new Date()
    newObj.setTime(obj.getTime())
    return newObj
  }

  // 对象或数组
  newObj = obj instanceof Array ? [] : {}
  if (newObj) {
    for (var i in obj) {
      newObj[i] =
        Object.prototype.toString.call(obj[i]) === '[object Object]'
          ? deepClone(obj[i])
          : obj[i]
    }
    return newObj
  }
}

export default deepClone;