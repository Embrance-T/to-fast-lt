/**
 *
 *
 * @desc 判断是否为 buffer类型
 * @param {*} a
 * @return {*} 
 */
export default function isArrayBuffer(a) {
  return a instanceof ArrayBuffer
}