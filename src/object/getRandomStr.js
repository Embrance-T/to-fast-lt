/**
 *
 *
 * @desc 生成随机字符
 * @return {*} 
 */
export default function getRandomStr() {
  return Math.random().toString(36).substr(3)
}