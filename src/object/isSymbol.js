/**
 *
 *
 * @desc 判断是否为Symbol类型
 * @param {*} a
 * @return {*} 
 */
export default function isSymbolType(a) {
  let type = Object.prototype.toString.call(a)
  return type == '[object Symbol]'
}