/**
* @description 复制内容
* @param   {String | Number}
*/
export default function copyValue(value = '', callback) {
  try {
    let str = value;
    let oInput = document.createElement("input");

    oInput.value = str;
    document.body.appendChild(oInput);
    oInput.select();
    document.execCommand("Copy");
    oInput.style.display = "none";
    callback(true);
  } catch (error) {
    callback(false);
  }
}