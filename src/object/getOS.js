/**
 * @description 获取当前操作系统
 */
export default function getOS() {
  let u = navigator.userAgent;

  if (!!u.match(/compatible/i) || u.match(/Windows/i)) {
    return 'windows';
  } else if (!!u.match(/Macintosh/i) || u.match(/MacIntel/i)) {
    return 'macOS';
  } else if (!!u.match(/iphone/i) || u.match(/Ipad/i)) {
    return 'ios';
  } else if (!!u.match(/android/i)) {
    return 'android';
  } else {
    return 'other';
  }
}