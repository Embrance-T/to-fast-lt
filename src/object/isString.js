/**
 *
 *
 * @desc 判断是否为 String类型
 * @param {*} str
 * @return {*} 
 */
export default function isString(str) {
  let type = Object.prototype.toString.call(str);
  return type == '[object String]';
}