/**
 *
 *
 * @desc 判断是否为 Undefined类型
 * @param {*} a
 * @return {*} 
 */
export default function isUndefined(a) {
  let type = Object.prototype.toString.call(a);
  return type == '[object Undefined]';
}