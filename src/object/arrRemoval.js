/**
 *
 *
 * @desc 数组去重、针对数组中某个对象的字段去重
 * @param {arr: 要去重的数组、 filed: 数组对象中要去重的 key}
 * @param {string} [filed='']
 */
export default function arrRemoval(arr = [], filed = '') {
  let person = arr
  let obj = {}
  person = person.reduce((pre, item) => {
    obj[item[filed]] ? '' : obj[item[filed]] = true && pre.push(item)
    return pre
  }, [])
}